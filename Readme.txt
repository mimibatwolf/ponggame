
Create a Pong game from scratch: http://www.ponggame.org/



Source code which consists of a single Readme.txt file is at https://bitbucket.org/yusufpisan/ponggame/

===========================================

2015 Marking Scheme for Pong Game in Quiz-1

Marking

(2 marks)	Both paddles displayed on screen. Left paddle moves by pressing W and S
		keys. Right paddle does not move.
(1 mark)	Left paddle is always visible on screen, does not exit the screen.
(1 mark)	Score of 0 - 0 is displayed. Score does not have to work for this mark.
(2 marks)	When “spacebar” is pressed, ball appears in the middle of the screen and
		travels towards the left side of the screen
(2 marks)	If the left paddle collides with the ball, score is increased from 0 to 1.
You do not have to implement the ball bouncing from the paddle.
(2 marks)	Pressing “R”, resets the score and restarts the game.

Total: 10 marks

===========================================


You need to create a "Pull Request" in BitBucket.
See https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html if necessary.


About Forking
      When you work with another user's public Bitbucket repository, typically you have read access to the code but not write access. This is where the concept of forking comes in. Here's how it works:
      Fork the repository to copy it to your own account.
      Clone the forked repository from Bitbucket to your local system.
      Make changes to the local repository.
      Push the changes to your forked repository on Bitbucket.
      Create a pull request from the original repository you forked to add the changes you made.
      Wait for the repository owner to accept or reject your changes.


When using version control with Unity

You need to make the Meta files visible in Unity
    * Edit >Project Settings > Editor -> Version Control    Visible Meta Files 

Add the .gitignore file
    * http://kleber-swf.com/the-definitive-gitignore-for-unity-projects/ Depending on your operating system, the .gitignore file may not be visible in the Explorer, Finder, etc but you can see it at the DOS and Unix shells.

Make sure your repository is readable by 'yusufpisan'

This project already has visible meta files and a .gitignore file (as well as a visible-gitignore.txt to remind you to always add .gitignore
