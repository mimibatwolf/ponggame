﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public int speed;	// the paddle's speed

	void Start(){
	}

	void FixedUpdate()
	{
		// move based on the input from the player
		float y = Input.GetAxisRaw ("Vertical");
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, y) * speed;
	}
		
}
