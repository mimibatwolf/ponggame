﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallBehaviour : MonoBehaviour {

	public int speed;			// the speed of the ball
	public Text scoreText;		// the score text

	Rigidbody2D rb;				// the ball's rigidbody
	bool isPlaying;				// if the game is currently being played
	int playerScore;			// the player's score
	Renderer renderer; 			// the ball's renderer

	void Start () {

		// get necessary components 
		rb = GetComponent<Rigidbody2D> ();
		renderer = GetComponent<Renderer> ();
		Init ();
	}

	void Init()
	{

		// initialise variables - the player's score is 0, the game has not started and the ball is in the start position, not moving
		playerScore = 0;
		isPlaying = false;
		this.gameObject.transform.position = Vector3.zero;
		renderer.enabled = false;
		rb.velocity = Vector2.zero;
	}

	void Update(){

		// if the game isnt currently playing, start it
		if (!isPlaying) {
			StartGame ();
		}

		// if 'r' is pressed, reset the game
		if (Input.GetKeyDown (KeyCode.R)) {
			Init ();
		}

		// update the score
		scoreText.text = "Score " + playerScore.ToString() + " - 0";

	}

	void StartGame(){

		// if 'space' is pressed, start the game by moving the ball
		if (Input.GetKeyDown(KeyCode.Space))
		{
			renderer.enabled = true;
			rb.velocity = Vector2.left * speed;
			isPlaying = true;
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		// if the ball collides with the player's paddle, add to their score
		if (col.gameObject.tag == "Player") {
			playerScore++;
		}
	}

}
